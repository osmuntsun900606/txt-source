「⋯⋯ 麻里子殿？　為何要把眼睛瞪的那麼大盯著人家的臉看」

米蘭達在大張著嘴呆愣在那裡的麻里子眼前揮了揮手。

「哎？　啊，不是，米蘭達桑是公主大人，吶」

解除了僵直的麻里子絲毫沒做思考就將剛剛想著的事情直接說了出來。

「公主大人⋯⋯⋯雖說是有像阿德雷那樣這麼稱呼的人，但我確實是沒有說過⋯⋯⋯可這又有什麼問題嗎？」
「問題⋯⋯ 誒都，就像是和我現在這樣輕鬆的交流真的可以麼，之類的」
「哎？」

這次換做是米蘭達一副發愣的樣子。

「麻里子殿。要怎麼想才會變成這樣我不是很明白，但不用在意這個的哦？　門之守衛的女兒怎麼會被特殊對待」
「是那樣嗎？」

雖然不清楚米蘭達的個人見解到何種程度，但這裡關於顯貴之人的概念似乎和日本有些不太一樣，麻里子這樣想著。

「考慮下看看。要是像塔莉婭大人那樣自己發現了轉移門的人，本人是會被認為是特別的。但是，僅僅因為是那個人的子孫，那能不能被稱得上是偉大的人就不得而知了？」
「那個嘛，確實是那樣」
「所以，沒有必要對我進行特殊對待。我又沒有什麼了不起的成就。倒不如說，同為門之守衛的女兒，作為後繼者確實做出了工作實業的薩妮婭殿應該被特別的看待才對」
「啊！是那樣。薩妮婭桑」

比起出身更看重實績，一邊聽著麻里子一邊這麼思考著，當薩妮婭的名字出現的時候才第一次想到那裡。從立場上薩妮婭也是處在公主的位置上。麻里子想起了薩妮婭散發出的那略微悠閑的氣氛。

（第一次見面的時候，就是以亞里亞母親的身份認識的吶。但確實是公主，或者說是大小姐）

「啊啊，薩妮婭殿麼。要說是立場相似的話，這麼說也對。總是能讓人依賴呢。都是門之守衛的女兒，弟弟的事情，身材和身高，總是不缺談論的話題呢」

米蘭達掰著手指頭，看上去很開心的列舉著和薩妮婭的共同點。可是，麻里子被其中一件事吸引住了。

「但是米蘭達桑，薩妮婭桑有弟弟的嗎？」

麻里子禁不住回頭看了眼塔莉婭。

「哎，沒說過的嗎？　有的哦，下面還有兩個弟弟」
「第一次聽說」
「其中一個是叫西蒙，最近剛剛結婚吧。現在兩個人就在鄰村生活著。他娶的塞茜就是住在這裡的姑娘。然後，另一個是叫塞爾曼，正做著探險者的工作。以我們的村子為據點，應該過幾天就會回來了吧」

從眯著眼睛述說著兒子們的塔莉婭的神情之中能感受到她對兒子們的愛。看到那副表情讓麻里子略微想起了祖母和母親。

「塞茜殿的廚藝很好。當她結婚的時候就辭掉了店裡的工作，而當村裡人知道了對象就是西蒙的時候還掀起了軒然大波呢」
「是那樣的。啊啊，說起來塞茜的結婚和麻里子還稍微有點關係」
「哎？」

被人說出有關係讓麻里子歪起了腦袋。那時還沒有來會有怎樣的關係呢。

「塞茜和兒子離開之後，住宿的員工就剩下了米蘭達一個人。原本，還在想著該怎麼辦才好，算上薩妮婭也就才只有３個人，所以才說著要打算招人」
「啊啊，是那麼一回事嗎。真是不可思議的緣分吶」

為了填補離開人的位置，下一個人到來。不僅僅是在公司只要是組織在哪裡都會發生，這不是什麼稀罕的事情。可是，那個接替那個位置的人是不太明白其存在的自己這件事，讓麻里子感到了一種不可思議的心情。


◇

「吶，托爾斯頓」
「咋了？」

將身體沉浸在澡盆之中，巴爾特向坐在旁邊的托爾斯頓搭著話。在巴爾特的視線前方的，是洗的滿身泡泡的阿德雷五人組的身姿。

「那個，果然還是想摸一摸，不這麼想麼？」
「要是俺的話可不這麼想，要不你去試試拜託米蘭達醬唄」
「那樣不就會招來誤解了嗎？」
「你要是把阿德雷推倒了再一陣揉搓，想必會被誤解的更厲害吧？」
「這樣啊」
「你要是想讓一部分婦女感到高興的話就上，別停下」
「⋯⋯這樣啊」

把鼻子都一起沉入水中咕嚕咕嚕地吐著泡泡，巴爾特滿臉遺憾的眺望著眼前那來回擺動的五組貓耳和貓尾。


════════════════════

這回沒有探險者出來！途中注意到了，突入進了男子浴池⋯⋯結果還是寫了（汗）。誰會得到利益呢⋯⋯⋯