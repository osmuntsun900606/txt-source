# 含有日文的章節段落

[TOC]

## 00020_第２章：異世界の日常/第１６話　最初的朋友

- 莉莉跟我一樣，光著腳在木地板上ペタペタ地走著。


## 00020_第２章：異世界の日常/第２１話　歓迎来到冒険者公会！（１）

- 只是，在這裡點單的過程也不是特別不同，好像不能決定的話，店員會過來幫忙下單。啊，話說チップ是什麼吧？


## 00020_第２章：異世界の日常/第３３話　暴走獣多鲁托斯

- 「Hyu──Dosu！」（ヒュッ──ドスッ！）
- 「喻──」（「うおっ──」）


## 00050_第５章：イルズ炎上/第６１話　伊鲁兹的大火（３）

- （Mcbthree： 天上よりご照覧あれ 祈願能被神所眷望
- Mcbthree： キルヴァンもコルウスも、聖域に住まう妖精と爭いが起こることをすでに理解していた。奇爾萬和葛爾烏斯都，已經理解到和住在聖域的妖精發生了紛爭。）


## 00050_第５章：イルズ炎上/第６８話　解咒

- 感覺開始恢復的指尖，再次握柄篭める力。


## 00060_第６章：スパーダへ/第８４話　和睦

- 在今天的戰鬥裡用了『邪心防壁（デス?ウォルデファン）』擋在前方的就是他。
- 莫因為有著死神風格的樣子，所以在公會裡都以『暗魔術士（ダーク?ウィザード）』來稱呼他，雖然確實給人第一印象是邪惡的化身，


## 00080_第８章：アルザス防衛戦/第１０６話　妖精ＶＳ天馬騎士（１）

- 忘れがちですが、幼女莉莉はほとんど空を飛べません。第４６話『林檎箱の謎』と第６３話『光の泉（１）』でその旨が書かれてますね。
- ついでに、少女莉莉が飛行可能な事は、光の泉から伊魯茲村まで飛んできた６８話『解呪』で明らかになっています。


## 00100_第１０章：魔王と勇者/第１６０話　守護之力（３）

- 總之，讓他再繼續亂揮的話很麻煩，所以用黒色魔力聚成漩渦的右拳，打碎了「パイルバンカー」鋼的刀身。


## 00140_第１４章：魔女は恋なんてしない/第２１８話　初次約会（１）

- 膝蓋下面穿的不是平時的黒色長靴，而是白色高筒襪和平底皮鞋（ローファーLoafer 平底皮鞋，日本學生制服搭配的皮鞋。）。（無盡：美女你誰啊？）


## 00160_第１６章：天使と悪魔/第２６１話　討伐盗賊的報酬

- （注：男主以前穿著那件是由巴風特的皮所制成的惡魔的擁抱「巴風特・エンブレス」，現在這件是「迪亞波羅・エンブレス」）


## 00160_第１６章：天使と悪魔/第２８９話　魔眼覺醒

- 「死吧」（うるせぇよ）


## 00170_第１７章：１４日間/第３０４話　白金的月２１日・伊斯基亞丘陵（１）

- 狼型的怪物的溫德魯駝著一樣是綠色的身體的哥布林奔馳著，有名的地龍裝載著複數的大塊頭獸人，健壯的四足發出ドシドシ的鳴響突進。


## 00170_第１７章：１４日間/第３０５話　白金的月２１日・伊斯基亞丘陵（２）

- 「您是知道沉默羊（注音：寂靜滲流，讀作サイレントシープ是英語Silent Sheep的音譯）的固有魔法吧？」


## 00170_第１７章：１４日間/第３１５話　白金之月２２日？ ？ ？（２）

- 「──安蒂米歐（エンディミオン），侍奉了舊魔王的黒魔女喲。」


## 00170_第１７章：１４日間/第３１７話　白金之月２６日・斯巴達王城

- いよいよ、第１６章に時間が追いつきました。次回で第１７章は完結です。
- 來週から、更新は月曜と金曜の二回に戻ります。よろしくお願いします。


## 00170_第１７章：１４日間/第３１８話　白金之月２６日・斯巴達

- 第１７章はこれで完結です。長い間、主人公不在で申し訳ありませんでした。
- それでは、次章もお樂しみに！


## 00180_第１８章：怠惰の軍勢/第３３４話　怠惰覺醒

- 「يرتجف، وهدير الرعد إلى الأرض（震えよ、大地に轟く雷鳴を）」


## 00180_第１８章：怠惰の軍勢/第３３６話　第二次的試炼

- 前話『ファーストキス』について、あまりに批判意見が殺到したので、まえがきに注意を追加しました。
- この話は、さらに前話となる３３４話『目覺める怠惰』にて、ほぼ無傷で助かった夏洛特との對比と、彼女の獨斷專行による被害を明確に表すためのものとなりました。
- また、長々と觸手攻めシーンを描いたのは、確かに省こうと思えば省ける部分ではありましたが、それでは全く「傳わらない」と思い、同時に、こういった不快なシーンも省かずに描き切る、というのが『黒の魔王』の作風だと考えるからです。
- しかしながら、私自身も作品の中において絶對的なタブーを設定しておりますので、その筆頭であるメインヒロインの凌辱・寢取られを描くことは決してありません。
- ですので、觸手モンスターに辱められる、という役割を男の西蒙に擔わせることで、タブーを回避しつつ、死亡に次ぐ鬱要素である凌辱を、自分が納得する形で描きました。
- 僅かながらネタバレとなりますが、西蒙が正式にヒロイン化することは（女體化することも、黒乃がバイになることも）ありません。ですので、この先のＢＬ展開を危懼された方は、どうぞ安心してお讀みください。


## 00180_第１８章：怠惰の軍勢/第３４６話　代達羅斯的叛乱

- 第１８章はこれにて完結です。


## 00190_第１９章：ランク５冒険者/第３４９話　帰宅

- この度、黒の魔王の書籍化が決定しました。林檎プロモーションのフリーダムノベルからの出版となります。
- ストーリーは同じですが、序盤の冗長だった部分や、第三章の問題改善など、大幅に話の密度を上げ、同じシーンでも書き直し、ほぼリメイクのような作りとなっています。また、ストーリーそのものに大きな影響はありませんが、なろう版ではなかった新規シーンなども追加されておりますので、どうぞお樂しみに。
- また、書籍化にあたって、なろう版の削除やダイジェスト化などはいたしません。全てそのまま殘し、連載もしばらくは今の週二回更新を維持しますので、ご安心ください。
- それでは、これからも黒の魔王をよろしくお願いいたします！


## 00190_第１９章：ランク５冒険者/第３６０話　超人剣技

- Ｑ　親衛隊って伊斯基亞古城にいたの？
- ヘレンが第２７５話『お友達（１）』にて、妮露に話しかけたクラスメイトであると明らかになった以上、時系列的にヘレンは野外演習に參加していない（教室のシーン時點で、野外演習組みは出發している）ことが証明されています。
- Ａ　フルバーストは黒乃が勝手に命名した技名ですので、射擊用語ではありません。感想でフォローしてくれた方、どうもありがとうございました。
- Ｑ　オーク（豚）とオーガ（鬼）のイメージを混同していませんか？
- Ａ　『ぜつおんなた「くびたち」』です。申し訳ありません、初出の第２０５話にて、振り仮名をふっていませんでした。


## 00190_第１９章：ランク５冒険者/第３６５話　紅翼伯爵の秘密（２）

- 黒乃真奈って誰だっけ⋯という方は、序章をご覧ください。
- そして、５月１４日は『黒の魔王』の初投稿日であることも、ついでにご確認ください。


## 00200_第２０章：色欲の世界/第３９６話　その一報

- 第２０章はこれで完結です。
- さて、いよいよ二年ぶりくらいに十字軍が動き始めてくれたようです。慘敗を喫した阿爾薩斯の戰いより、黒乃が再び十字軍に挑む！
- それでは、次章もお樂しみに。


## 00210_第２１章：斯巴達開戦/第３９７話　格里高利这个男人（１）

- 第２１章スタートです。
- さて、この諾爾茲司祭長を覺えている方はどれだけいらっしゃるでしょうか。誰このオッサン？　という方は、第７４話『イヤな女（１）』をご覧ください。この話が初登場で、以後、阿爾薩斯の戰いが終わるまで、十字軍側の事情を語ってくれる都合のいいオジサンです。


## 00210_第２１章：斯巴達開戦/第３９８話　格里高利这个男人（２）

- ちなみに、艾絲蒂爾は第１０６話『妖精ＶＳ天馬騎士』にて初登場となっております。


## 00210_第２１章：斯巴達開戦/第３９９話　第四隊『剣闘士』

- これまで、何とか週２ペースを維持できていましたが、いよいよ殘りのストックが心もとなくなってきましたので、ここでペースを落とさざるをえませんでした。
- ついでに、あまり詳しい個人の事情を語るつもりはありませんが、私自身、一日中執筆に時間を割ける身分ではありませんので、これまで以上に執筆量を増やすことも難しいです。自分の生活の中で、書ける時間というのは限られていますが、それでも、せめて連載は週１回よりは落としたくないので、このペースで頑張りたいと思います。
- それでは、これからも『黒の魔王』をよろしくお願いいたします。


## 00210_第２１章：斯巴達開戦/第４００話　対沙利叶作戦会議

- 早いもので、４００話です！ここまで應援してくださった讀者の皆樣、本當にどうもありがとうございます。どうぞ、これからも『黒の魔王』をよろしくお願いします！
- この話は時系列的に色欲玫瑰討伐前となりますが、致命的に矛盾が發生する部分（色欲玫瑰討伐後でなければ分からない台詞・地の文）があったので、修正しました。


## 00210_第２１章：斯巴達開戦/第４０２話　初次約会

- アマゾンにて『黒の魔王』二卷が予約開始しました！


## 00210_第２１章：斯巴達開戦/第４０４話　多数決之原理

- 妮露はヤンデレベルが上がっている


## 00210_第２１章：斯巴達開戦/第４０７話　羽翼折断之時

- いよいよ明日、書籍版『黒の魔王』の二卷が發賣となります。どうぞ、よろしくお願いいたします！


## 00210_第２１章：斯巴達開戦/第４１０話　剣闘士

- あけまして、おめでとうございます！
- それでは、今年も黒の魔王と病嬌を、よろしくお願いします！


## 00210_第２１章：斯巴達開戦/第４１２話　加拉哈德要塞

- それでは、次回こそついに開戰、ですので、どうぞお樂しみに！


## 00220_第２２章：第五次加拉哈德戦争/第４１３話　開戦・第五次加拉哈德戦争

- 次章こそ、本當に戰爭が始まるので、どうぞお樂しみ！


## 00220_第２２章：第五次加拉哈德戦争/第４１６話　垂直戦線（バーティカルリミット）（１）

- それでは、引き續き黒乃の立體機ど──もとい、城壁での戰闘をお樂しみください！


## 00220_第２２章：第五次加拉哈德戦争/第４１８話　アルターフェイス

- ちなみに、妮露が『Alter・Face』として參加していれば、偽名はエルとなってました。わたしがＬです。


## 00220_第２２章：第五次加拉哈德戦争/第４３０話　傷心の帰国

- それでは、これからも黒の魔王をよろしくお願いいたします！


## 00220_第２２章：第五次加拉哈德戦争/第４３１話　伯爵令嬢

- 次回、第２３章より、月曜と金曜の週二回更新に戻します！
- 理由としては、自分でも思った以上に加拉哈德戰爭のストーリーの進みが遅い⋯という殘念なものですが、どうぞこれからもお付き合いいただければ幸いです。一應、ストックの方は最低限は稼いでいるので、２３章以後の更新についてもご安心ください。
- この戰いの結末がどうなるか、というのは勿論、まだ語るわけにはいきませんが、それでも、決著が『黒の魔王』のストーリーにおいてこれまでにないほど大きな意味を持つことになるのは間違いありません。どうぞご期待ください！


## 00230_第２３章：ヘルベチアの聖少女/第４４２話　侵食する悪夢（３）

- 今話において、子供が殺される描寫がありました。もし、不快に感じる方がいましたら、申し訳ありません。ほとんど分かり切った展開ではありますが、一應、內容のネタバレは控えたいので、前書きで注意は書きませんでした。


## 00230_第２３章：ヘルベチアの聖少女/第４４６話　仮面の男

- Ｘ「はぁ～個人的な理由あるから琳菲露德助けるわ～黒乃の手柄台無しになるけど、個人的な理由だからしょうがないわ～俺は自分の意思を貫くわ～」チラッ


## 00240_第２４章：聖夜決戦/第４４９話　騒乱の開幕

- 第２４章からは、また週一回の更新ペースに落とさせていただきます。大變、申し訳ありません。
- ただ、話の流れによっては、週に二回とすることもあるかもしれません。とりあえず、次回は８月４日の月曜日にも更新させていただきます。
- あと、どうでもいい裏設定について。
- インキュバスは眠った女性に淫夢を見せて、效率的に生命吸收（ドレイン
- そして、インキュバスに襲われた不運な女性は、そこそこに生命力を吸收されるが、自分の願望全開な素敵な淫夢のお蔭で物淒いスッキリ目覺めることができます。インキュバスさんマジ紳士。


## 00240_第２４章：聖夜決戦/第４７２話　聖夜の神託

- それでは、よいお年を！


## 00250_第２５章：偽りの日々/第４７３話　第五次加拉哈德戦争の終結

- 注一 應該是 十五之夜 原文 盜んだバイクで走り出す 騎著偷來的機車飛馳 １９８５年的歌 我查了半小時 敢信？BY九條
- 新年、あけましておめでとうございます！
- 恐らく、今年一年連載しても完結の見込みはありませんが⋯どうぞ、今年も『黒の魔王』をよろしくおねがいいたします！


## 00250_第２５章：偽りの日々/第４８０話　悪魔の取引

- 注三：原文 スティンガー（美）毒刺地對空導彈


## 00250_第２５章：偽りの日々/第４９８話　嵐の前の静けさ

- 第２５章はこれで最終回です。
- 感想欄でちら嚯啦と予想されていた方はいましたが、やってきました第五試練。
- それでは、次章もお樂しみに。


## 00260_第２６章：暴食の嵐/第５０３話　誤判退機（１）

- １０メートル級の大タコのイメージは、宇宙戰爭のトライポッドです。でも正確に言なら、このトライポッドを元にしたであろう、地球防衛軍２という遊戲に登場する宇宙人のロボ・新型歩行戰車『ディロイ』ですね。


## 00260_第２６章：暴食の嵐/第５０９話　前往暴風中心

- ようやく、書籍版『黒の魔王』第４卷の發賣が決定しました。發賣日は９月１日です。みな桑、どうぞよろしくお願いいたします！


## 00260_第２６章：暴食の嵐/第５１４話　自己能做到的事

- 現在のストーリーは、勿論、今後のストーリー展開に必要なモノであると自分では考え拔いた末にできたものです。しかし、これまでの話よりも、讀者の方に滿足いただけるほどの面白さが出せなかった、というのはひとえに私の不德と致すところです。
- さて、お詫びとは別のお話になりますが、今回、ついに沙利叶が新たな加護を授かりました。ですが、「沙利叶は暗黒騎士芙莉西亞の加護を授かるだろう」との予想は、前々からかなり多かったので、私としては予定調和すぎると思われるかと戰々恐々。しかし、沙利叶には芙莉西亞の加護でなければいけない（設定上）ので、予想されたからといって變更するという捻くれた對應はしません。というか、何でみんな分かるんだよ、という思いの方が強いですね（苦笑）
- ともかく、今章も殘すところあと僅か。もう少しだけ第五試練にお付き合いいただき、次章での新たな展開を、樂しみにしていただければ幸いです。
- それでは、これからも『黒の魔王』をよろしくお願いいたします。


## 00260_第２６章：暴食の嵐/第５１５話　暗黒騎士沙利叶

- 前回のあとがきを受けて、多くの方から勵ましの書き込みをいただきました。本當にありがとうございます。
- あらためて明言しておきたいのですが、今回の措置は私自身も「早く更新した方がいいな」という判斷に至ったので、決して批判意見に折れた、ということではありません。また、ストーリーの流れなども、すでに書き上げたものから修正は一切しておりません。
- それと、連續更新でストック大丈夫ですか、というお聲もありましたが、あえて答えます。
- というワケで、次章と次々章まではすでに書き上がっている段階なので、ストックにはそれなりに餘裕があります。周一更新のお蔭で、安定的にストックを溜めて行けるので、これからも定期更新は問題なく續けられるかと思いますので、どうぞご心配なく。


## 00260_第２６章：暴食の嵐/第５１７話　自由へ向かって

- これにて、第２６章は完結です。
- さて、次章は待望の莉莉と菲奧娜による修羅場となりますが⋯先に斷わっておくと、あと二話ほどお待ちいただくことになります。申し訳ありませんが、他にいれられるタイミングのない話でしたので。今週の金曜と、月曜もまた更新しますので、本番は來週の金曜となる予定です。
- それでは、黒の魔王、始まって以來の最大の修羅場が、ついに黒乃に襲い掛かる第２７章を、お樂しみに！


## 00270_第２７章：アイシテル/第５３２話　暴君の後継者

- 在那裡的並不是浮現ヘラヘラ的軟弱笑容像是微笑嬌生慣養的少爺的代表一樣的男人，而是漆黒的巨大的鎧甲站立著。
- 「ガシャリ」伴隨著沉重的金屬聲。『魔王之鎧艾璐羅德・姫婭』向前邁出一步。
- 吐在我面前的，並不是什麼因苦悶而歪曲表情地小少爺般地醜陋死體，而是「バラバラ」地散落著的骨頭殘骸。連一根頭髮也好，一滴血也罷都沒有殘留的，乾燥至極的白骨死屍。
- 那樣地被低聲私語的瞬間，我聽到了經歷千年之後首次「ドクン」這樣自己心臟的鼓動。


## 00270_第２７章：アイシテル/第５３３話　王様のお値段

- 「嗯，你最。儂也暴走了的時候，是也沒有停停止破損和覺悟了，完整地回來的是感謝的念頭絶えん。」


## 00270_第２７章：アイシテル/第５３６話　楽園

- それでは、次回もお樂しみに＆良いお年を！


## 00280_第２８章：罪を重ねて/第５３７話　王子の帰還

- 新年、あけましておめでとうございます。どうぞ今年も、『黒の魔王』をよろしくお願いいたします！


## 00280_第２８章：罪を重ねて/第５４８話　恋人（２）・修正版

- この話は利用規約に抵觸する恐れがあるので、內容を一部削除した上で公開されております。該當箇所をそのまま削除しているだけですので、話の繫がりは途切れています。
- この話の完全版は、ノクターンの『黒の魔王・裏』にて掲載されておりますので、そちらをご利用ください。


## 00290_第３０章：妖精殺し/第５８６話　頂上決戦

- （原文「いやぁあああああああああああああああああああああああああああああっ！！」）


## 00290_第３０章：妖精殺し/第５８９話　莉莉ＶＳ菲奧娜（３）

- 「啊啊，疼的要死啊⋯⋯フェンフ，快給我『妖精的秘藥』」
- それでは皆桑、よいお年を！


## 00290_第３０章：妖精殺し/第５９０話　最後の試練

- 新年、あけましておめでとうございます。どうぞ、今年も『黒の魔王』をよろしくおねがいいたします。
- というワケで、第３０章はこれで完結です。年も開けて、氣持ちも新たに、黒乃にはまず最後の試練に挑んでもらいたいと思います。それでは、どうぞお樂しみに。


## 00300_第３１章：嫉妬の女王/第６１８話　試練の果て

- ようやく、七つもあった試練イベントを終え、さらに待望のヤンデレハーレムルート解禁ということで、加拉哈德戰争に次いで、また一つ、物語の大きな山を越えた氣がします。
- 詳しいことは、近い内に活動報告にて書きたいと思います。
- それでは、これからも『黒の魔王』をよろしくお願いします！


## 00310_第３２章：修道会の影/第６１９話　灰色の取引

- さて、病嬌大戰を乗り越えて、いよいよ本格的に十字軍との戰いが始まる⋯かも、しれない、新章突入です！
- 今回のお話は、時系列としては第２７章５７２話『阿瓦隆交差点』の直後ということになります。讀んで流れが思い出せなかった方は、讀み返してみると理解が捗るかと。


## 90010_作者活動報告/漫畫/00010_コミック版『黒の魔王』第３話感想会場（石）

- コミック版『黒の魔王』第３話感想會場（石）


## 90010_作者活動報告/漫畫/00020_祝・コミック一巻発売＆第６話感想会場

- 祝・コミック一卷發賣＆第６話感想會場


## 90010_作者活動報告/漫畫/00030_第１０話

- 奇爾萬本人實力大概是等級４左右，因此，完整詠唱的話是能發射『大閃光砲（ルクス・フォースブラスト）』的。
- 「──這是『火盾（イグニス・シルド）』」
